'use strict';
const Cart = require('../app/cart.js');
const BundleDiscount = require('../app/discounts/bundle.js');
const WholesaleDiscount = require('../app/discounts/wholesale.js');
const AbsoluteDiscount = require('../app/discounts/absolute.js');
var assert = require('assert');

var priceTable = {
    'classic': 1,
    'standout': 2,
    'premium' : 4,
};

describe('Cart', function() {
  describe('#price()', function() {
    it('should be 0 for a newly created cart', function() {
      assert.equal(0, (new Cart()).price());
    });

    it('should equal the sum of prices of items in the cart', function(){
        let cart = new Cart(priceTable);
        cart.addItem('classic');
        cart.addItem('standout');
        cart.addItem('standout');
        cart.addItem('premium');
        cart.addItem('classic');
        assert.equal(10,cart.price());
    });

    it('should be able to apply a 3-for-2 bundle discount', function(){
        let classicThreeForTwo = new BundleDiscount({
            'type': 'classic',
            'size': 3,
            'amountToBill': 2,
        });
        let cart = new Cart(priceTable,[classicThreeForTwo]);
        cart.addItem('classic');
        cart.addItem('classic');
        cart.addItem('classic');
        assert.equal(2, cart.price());
    });

    it('should be able to apply a 3-onwards wholesale discount', function(){
        let take3forCheaper = new WholesaleDiscount({
            'type':'premium',
            'threshold':3,
            'price':2,
        });
        let cart = new Cart(priceTable, [take3forCheaper]);
        cart.addItem('premium');
        cart.addItem('premium');
        cart.addItem('premium');
        assert.equal(6, cart.price());
    });

    it('should be able to apply an absolute discount', function(){
        let cheaperPremium = new AbsoluteDiscount({
            'type':'premium',
            'price':3,
        });
        let cart = new Cart(priceTable, [cheaperPremium]);
        cart.addItem('premium');
        cart.addItem('premium');
        cart.addItem('premium');
        assert.equal(9, cart.price());
    });
  });
});

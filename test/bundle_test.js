'use strict';
const Bundle = require('../app/discounts/bundle.js');
var assert = require('assert');

var defaultConfig = {
    'type': 'classic',
    'size': 3,
    'amountToBill': 2,
};

describe('Bundle Discount', function() {
    describe('#billedQuantity()', function() {
        it('should be 0 for no products', function() {
            assert.equal(0, (new Bundle(defaultConfig)).billedQuantity(0));
        });

        it('should be the same for fewer products than bundle size', function(){
            assert.equal(2, (new Bundle(defaultConfig)).billedQuantity(2));
        });

        it('should bill a lesser amount when bundle size is reached', function(){
            assert.equal(2, (new Bundle(defaultConfig)).billedQuantity(3));
        });

        it('should form only one bundle if the size is surpassed only once', function(){
            assert.equal(3, (new Bundle(defaultConfig)).billedQuantity(4));
        });

        it('should form more than one bundle if the size is surpassed more than once', function(){
            assert.equal(5, (new Bundle(defaultConfig)).billedQuantity(7));
        });
    });
});

'use strict';
const AbsoluteDiscount = require('../app/discounts/absolute.js');
var assert = require('assert');

var defaultConfig = {
    'type': 'standout',
    'price': 2,
};

describe('Absolute Discount', function() {
    describe('#price()',function() {
        it('should return discounted price when discounted price is lower than original price',function() {
            assert.equal((new AbsoluteDiscount(defaultConfig)).price(3),2);
        });
        it('should return original price when discounted price is higher than original price', function() {
            assert.equal((new AbsoluteDiscount(defaultConfig)).price(1),1);
        });
    });
});

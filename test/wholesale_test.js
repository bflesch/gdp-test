'use strict';
const WholesaleDiscount = require('../app/discounts/wholesale.js');
var assert = require('assert');

var defaultConfig = {
    'type': 'premium',
    'threshold': 3,
    'price': 1,
};

describe('Wholesale Discount', function() {
    describe('#discountApplies()', function() {
        it('should be false for no products', function() {
            assert.equal((new WholesaleDiscount(defaultConfig)).discountApplies(0), false);
        });
        
        it('should be false for less products than threshold', function() {
            assert.equal((new WholesaleDiscount(defaultConfig)).discountApplies(2), false);
        });

        it('should be true for same amount of products as threshold', function() {
            assert.equal((new WholesaleDiscount(defaultConfig)).discountApplies(3),true);
        }); 

        it('should be true for more products than threshold', function() {
            assert.equal((new WholesaleDiscount(defaultConfig)).discountApplies(8), true);
        });
    });
    describe('#price()',function() {
        it('should return discounted price when discount applies',function() {
            assert.equal((new WholesaleDiscount(defaultConfig)).price(3, 3),1);
        });
        it('should return original price when discount does not apply', function() {
            assert.equal((new WholesaleDiscount(defaultConfig)).price(2, 3),3);
        });
    });
});

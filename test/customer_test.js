'use strict';
const Customer = require('../app/customer.js');
const BundleDiscount = require('../app/discounts/bundle.js');
const WholesaleDiscount = require('../app/discounts/wholesale.js');
const AbsoluteDiscount = require('../app/discounts/absolute.js');
var assert = require('assert');


describe('Customer', function() {
    describe('#discounts', function() {
        it('should store the discounts defined for a customer', function() {
            let customer = new Customer([
                    {
                        'type': 'bundle',
                        'product': 'classic',
                        'size': 5,
                        'amountToBill': 4
                    },
                    {
                        'type': 'absolute',
                        'product': 'standout',
                        'price': 309.99
                    },
                    {
                        'type': 'wholesale',
                        'product': 'premium',
                        'size': 3,
                        'price': 389.99
                    },
            ]);
            assert.equal(customer.discounts.length, 3);
            assert.ok(customer.discounts[0] instanceof BundleDiscount);
            assert.ok(customer.discounts[1] instanceof AbsoluteDiscount);
            assert.ok(customer.discounts[2] instanceof WholesaleDiscount);
        });
    });
});

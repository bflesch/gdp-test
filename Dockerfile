FROM ubuntu:17.04

RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install ca-certificates -y
RUN apt-get install --no-install-recommends --no-install-suggests -y nodejs nodejs-legacy
RUN apt-get install --no-install-recommends --no-install-suggests -y npm

RUN mkdir /app

WORKDIR /app

COPY package.json package.json 

RUN npm install express --save
RUN npm install helmet --save

COPY app/ app/

CMD ["nodejs","app/index.js"]

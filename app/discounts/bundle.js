'use strict';

class Bundle {
    constructor(rules){
       this.type = rules.type;
       this.size = rules.size;
       this.amountToBill = rules.amountToBill;
    }

    billedQuantity(originalQuantity){
        let remainder = originalQuantity % this.size;
        let bundles = (originalQuantity - remainder)/this.size;
        return (bundles * this.amountToBill) + remainder;
    }

    applyDiscount(item){
        if (item.type !== this.type)
            return item;

        let quantity = item.quantity;
        let price = item.price;
        return {
            'type': item.type,
            'quantity': this.billedQuantity(quantity),
            'price': price
        };
    }
}

module.exports = Bundle;

'use strict';

class Wholesale {
    constructor(rules){
       this.type = rules.type;
       this.threshold = rules.threshold;
       this.promotionalPrice = rules.price;
    }

    discountApplies(quantity){
        return quantity >= this.threshold;
    }

    price(quantity,originalPrice){
        if (this.discountApplies(quantity))
            return this.promotionalPrice;
        return originalPrice;
    }

    applyDiscount(item){
        if (item.type !== this.type)
            return item;

        let quantity = item.quantity;
        let price = this.price(item.quantity, item.price);
        return {
            'type': item.type,
            'quantity': quantity,
            'price': price
        };
    }

}

module.exports = Wholesale;

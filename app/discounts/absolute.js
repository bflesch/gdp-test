'use strict';

class Absolute {
    constructor(rules){
       this.type = rules.type;
       this.promotionalPrice = rules.price;
    }

    discountApplies(price){
        return this.promotionalPrice < price;
    }

    price(originalPrice){
        if (this.discountApplies(originalPrice))
            return this.promotionalPrice;
        return originalPrice;
    }

    applyDiscount(item){
        if (item.type !== this.type)
            return item;

        let quantity = item.quantity;
        let price = this.price(item.price);
        return {
            'type': item.type,
            'quantity': quantity,
            'price': price
        };
    }
}

module.exports = Absolute;

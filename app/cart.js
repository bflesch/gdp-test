'use strict';
const BundleDiscount = require('../app/discounts/bundle.js');
const WholesaleDiscount = require('../app/discounts/wholesale.js');
const AbsoluteDiscount = require('../app/discounts/absolute.js');

class Cart {
    constructor(priceTable, discounts){
        this.inventory = {};
        this.priceTable = priceTable;
        this.discounts = {};
        if (discounts){
            for (let discount of discounts){
                this.discounts[discount.type] = discount;
            } 
        }
    }

    price(){
        let sum = 0;
        for (let type in this.inventory){
            let item = {
                'type': type,
                'quantity': this.inventory[type],
                'price': this.priceTable[type],
            };
            if (this.discounts[type]){
                item = this.discounts[type].applyDiscount(item);
            }
            sum += item.quantity * item.price;
        } 
        return sum;
    }

    addItem(item){
        if (!this.inventory[item]) {
            this.inventory[item] = 0;
        }
        this.inventory[item] += 1;
    }
}

module.exports = Cart;

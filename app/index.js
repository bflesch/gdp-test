'use strict';
const express = require('express');
const helmet = require('helmet');
const app = express();
const Cart = require('./cart.js');
const Customer = require('./customer.js');
const config = require('./config.js');

app.use(helmet());

var customers = {};
for(let customer in config.customerDiscounts){
    customers[customer] = new Customer(config.customerDiscounts[customer]);
}


app.get('/cart/:customer/:classic/:standout/:premium', function (req, res) {
    let customer = customers[req.params['customer']];
    let discounts = [];
    if (customer)
        discounts = customer.discounts;
    let cart = new Cart(
            config.priceTable,
            discounts
            );
    for(let product of ['classic','standout','premium']){
        for(let i = 0; i < req.params[product]; i++){
            cart.addItem(product);
        }
    }
    res.send("" + cart.price());
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});

'use strict';
let config = {
    priceTable: {
        'classic': 269.99,
        'standout': 322.99,
        'premium': 394.99
    },
    customerDiscounts: {
        'unilever': [
            {
                'type':'bundle',
                'product':'classic',
                'size': 3,
                'amountToBill': 2
            },
        ],
        'apple': [
            {
                'type': 'absolute',
                'product': 'standout',
                'price': 299.99
            },
        ],
        'nike': [
            {
                'type': 'wholesale',
                'product': 'premium',
                'threshold': 4,
                'price': 379.99
            },
        ],
        'ford': [
            {
                'type': 'bundle',
                'product': 'classic',
                'size': 5,
                'amountToBill': 4
            },
            {
                'type': 'absolute',
                'product': 'standout',
                'price': 309.99
            },
            {
                'type': 'wholesale',
                'product': 'premium',
                'threshold': 3,
                'price': 389.99
            },
        ],
    }
};

module.exports = config;

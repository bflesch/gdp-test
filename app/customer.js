'use strict';
const BundleDiscount = require('../app/discounts/bundle.js');
const WholesaleDiscount = require('../app/discounts/wholesale.js');
const AbsoluteDiscount = require('../app/discounts/absolute.js');

class Customer {
    constructor(discountsConfig){
        this.discounts = [];
        for (let discount of discountsConfig) {
           if (discount.type == 'bundle'){
               discount['type'] = discount['product'];
               this.discounts.push(new BundleDiscount(discount)) ;
               continue;
           }
           if (discount.type == 'absolute'){
               discount['type'] = discount['product'];
               this.discounts.push(new AbsoluteDiscount(discount) );
               continue;
           }
           if (discount.type == 'wholesale'){
               discount['type'] = discount['product'];
               this.discounts.push(new WholesaleDiscount(discount) );
               continue;
           }
        }
    }
}

module.exports = Customer;

# Job Ad Checkout App

## Requirements
- docker
- docker-compose

## Running Tests
```bash
docker-compose up --build test
```

## Running App
```
docker-compose up server
```
Starts the server on port 3000

## Endpoint
```
http://localhost:3000/cart/:customer/:classic/:standout/:premium
```
where `:customer` is the customer's name, in lowercase, and `:classic`, `:standard`, and `:premium` are the amount of items to be billed in that category, returns a printed number with the total price for that cart

###
Proposed future improvements

* Functional testing
    * validation of routes for customers
* Proper implementation of Customer class
* Proper routing (adhering to some standard)
* Session management
    * actually managing cart
* Some form of authentication
* Better customer management
    * documentation
    * simpler config file
    * live management (admin interface)
* Persistence of changes to customers made through admin

### Closing Remarks

It was a remarkable experience to try my hand at node.js, which I had never done before, and to use Docker from scratch.

I had major difficulties and was not able to attain the expected coding speed, but have learned quite a bit and expect to improve, in hopes to have an opportunity to present a better application in the future.

Best regards,
Breno
